# CS

CS: Connecting the student
We try to fix the gap between academic world and professionals world. We let you meet your professionals idols in virtual environment.

## Backend Guide

[README.md](code/backend/README.md)

[Live](https://mukezhz.pythonanywhere.com/fgraphql)

## Frontend Guide

[README.md](code/frontend/README.md)

[Live](https://csguy.netlify.app)
