# GUIDES TO START THIS PROJECTS

## Required dependencies:
  - python3.6 or above
  - postgresql

### Let's start the project

#### Clone the project
`git clone --depth=1 https://gitlab.com/willsir/cs.git`

#### Navigate to backend directory
`cd cs/code/backend`

#### Create virtual environment for python
`python -m venv virtualenv`

#### Activate virtual environment
```
for bash/zsh:
source ./virtualenv/bin/activate

for fish:
source ./virtualenv/bin/activate.fish
```

#### Install Depedencies
`pip install -r requirements.txt`

#### Install postgresql in your operating system
[Postgresql Installation](https://www.postgresql.org/download/)

#### Create user/database in postgresql and grant permission for that user
[Follow this guide](https://djangocentral.com/using-postgresql-with-django)

#### Create .env file in your django project directory and update the following
```
SECRET_KEY='sercret key of yours'
DOMAIN="*"
DEBUG=True
EMAIL=email
KHALTI_PUBLIC=publickey
KHALTI_PRIVATE=secretkey
DB_NAME=dbname
DB_USER=username
DB_PASSWORD=password
DB_HOST=localhost
DB_PORT=5432
```

#### Migrate tables in your postgresql db
`python manage.py makemigrations`

#### Start the project
`python manage.py runserver`

