"""Registering GoogleCredential to admin interface."""
from django.contrib import admin
from .models import GoogleCredential


admin.site.register(GoogleCredential)
