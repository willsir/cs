"""Functions related to the google."""
import requests
from pip._vendor import cachecontrol

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.http import JsonResponse

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google.oauth2 import id_token

from .utils import get_flow, credentials_to_dict, get_user_info, add_to_model
from .vars import CLIENT_SECRETS_FILE, SCOPES, CLIENT_ID, BASE_URI


def authorize(request):
    """For authorization also maintaining the session."""
    if request.session.items():
        url = BASE_URI
        user = request.session.get('name')
        res = {
            "msg": "Hi! " + user + " You're already authorized",
            "url": "Return Home: " + url,
        }
        return JsonResponse(res)
    flow = get_flow(CLIENT_SECRETS_FILE, SCOPES)
    authorization_url, state = flow.authorization_url(
        access_type="offline",
        include_granted_scopes="true",
    )
    request.session["state"] = state
    return redirect(authorization_url)


def callback(request):
    """Obtain the access token."""
    if not request.session.get("state"):
        return JsonResponse({
            "status": "error",
            "state": request.session.get("state")
        })
    state = request.session["state"]

    flow = get_flow(CLIENT_SECRETS_FILE, SCOPES, state)
    flow.fetch_token(authorization_response=request.build_absolute_uri())

    credentials = flow.credentials

    request_session = requests.session()
    cached_session = cachecontrol.CacheControl(request_session)
    token_request = Request(session=cached_session)
    request.session["credentials"] = credentials_to_dict(credentials)
    id_info = id_token.verify_oauth2_token(
        id_token=credentials.id_token,
        request=token_request,
        audience=CLIENT_ID
    )

    request.session["google_id"] = id_info.get("sub")
    request.session["name"] = id_info.get("name")

    user_info = get_user_info(credentials.token)
    user_id = user_info.get("id")
    email = user_info.get("email")
    username = email.split("@")[0]
    first_name = user_info.get("given_name")
    last_name = user_info.get("family_name")
    status = add_to_model(
         request,
         user_id=user_id,
         username=username,
         email=email,
         first_name=first_name,
         last_name=last_name
         )
    if status == "success":
        return redirect("test")
    return JsonResponse({
        "error": "True",
        "msg": "Operation Failed: " + status,
        })


def revoke(request):
    """Revoke credentials from google."""
    if "credentials" not in request.session:
        url = "http://localhost:8000/"
        res = {
            "msg": "Need to authorize before testing the code",
            "url": "Return Home: " + url,
        }
        return JsonResponse(res)

    credentials = Credentials(**request.session["credentials"])

    revoke = requests.post(
        "https://oauth2.googleapis.com/revoke",
        params={"token": credentials.token},
        headers={"content-type": "application/x-www-form-urlencoded"},
    )
    status_code = getattr(revoke, "status_code")
    res = {
        "status_code": status_code,
        "status": "",
        "msg": "",
        "credentials": request.session.get("credentials"),
    }

    res["status"], res["msg"] = (
        ("success", "Revoke is successful")
        if status_code == 200
        else ("fail", "unable to revoke, Please authorize again")
    )

    if status_code == 400:
        del request.session["credentials"]
        reverse_lazy(authorize)
    return JsonResponse(res)


# need to add decorator
def clear_credentials(request):
    """Session is completely destroyed."""
    if request.session.get("credentials"):
        request.session.clear()
    res = {
        "msg": "Your session has been cleared",
        "name": request.session.get("name"),
    }

    return JsonResponse(res)
