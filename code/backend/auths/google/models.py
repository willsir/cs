"""Model to store access token and refresh token of the user."""
from django.db import models

from users.models import CustomUser


class GoogleCredential(models.Model):
    """Credentials of google for persistent session."""

    user = models.OneToOneField(to=CustomUser, on_delete=models.CASCADE)
    token = models.CharField(
       verbose_name="Access Token",
       max_length=254,
       null=False
       )
    refresh_token = models.CharField(
       verbose_name="Token used to generate new access token",
       max_length=254,
       null=False
       )

    def __str__(self):
        """Return username of owner."""
        return CustomUser.objects.get(email=self.user.email).username
