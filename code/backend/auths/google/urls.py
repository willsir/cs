from django.urls import path

from .google_handler import (
    authorize,
    callback,
    clear_credentials,
    revoke,
)
from .views import test_api, index, logout

urlpatterns = [
    path("", index),
    path("authorize/", authorize, name="authorize"),
    path("revoke/", revoke, name="revoke"),
    path("clear/", clear_credentials, name="clear"),
    path("test/", test_api, name="test"),
    path("callback/", callback, name="callback"),
    path("logout/", logout, name="logout"),
]
