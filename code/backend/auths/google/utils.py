"""Utilites functions which helps to modularize the tasks."""

import requests
from requests.structures import CaseInsensitiveDict

from django.contrib.auth import login
from django.db.utils import IntegrityError

from google_auth_oauthlib.flow import Flow

from users.models import CustomUser, UserProfile
from .vars import CLIENT_SECRETS_FILE, SCOPES, CALLBACK_URI
from .models import GoogleCredential


def get_flow(client_secret_file, scopes, state=None):
    """Return the flow which is used to connect with google."""
    flow = Flow.from_client_secrets_file(
        client_secrets_file=CLIENT_SECRETS_FILE, scopes=SCOPES, state=state
    )
    flow.redirect_uri = CALLBACK_URI
    return flow


def credentials_to_dict(credentials):
    """Credentials is serialized."""
    return {
        "token": credentials.token,
        "refresh_token": credentials.refresh_token,
        "token_uri": credentials.token_uri,
        "client_id": credentials.client_id,
        "client_secret": credentials.client_secret,
        "scopes": credentials.scopes,
    }


def save_credentials(request, user):
    """Credentials is saved into database."""
    credentials = request.session.get("credentials")
    token = credentials.get("token")
    refresh_token = credentials.get("refresh_token")
    try:
        google = GoogleCredential.objects.create(
            user=user,
            token=token,
            refresh_token=refresh_token
        )
    except IntegrityError:
        # Need to write logic to obtain access token after it expires
        pass
    return google


def get_user_info(token):
    """User info is returned."""
    url = "https://www.googleapis.com/userinfo/v2/me"
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["Authorization"] = "Bearer " + token
    return requests.get(url, headers=headers).json()


class GoogleException(Exception):
    """Google Exception."""

    pass


def add_to_model(request, user_id=0, username=None, email=None,
                 first_name=None,
                 last_name=None):
    """Store data into database."""
    try:
        custom_user = CustomUser.objects.get(email=email)
        if custom_user is not None:
            return "User Already Exists"
        first_name = first_name
        last_name = last_name
        login_method = custom_user.userprofile.login_method
        if login_method != "google":
            raise GoogleException(f"Please login with {login_method}")
    except CustomUser.DoesNotExist:
        custom_user = CustomUser.objects.create(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name
            )
        user_profile = UserProfile.objects.create(user=custom_user)
        user_profile.login_method = "google"
        custom_user.set_unusable_password()
        custom_user.save()
        user_profile.save()
        save_credentials(request, custom_user)
    login(request,
          custom_user,
          backend="django.contrib.auth.backends.ModelBackend"
          )
    return "success"
