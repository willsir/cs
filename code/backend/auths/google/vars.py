"""Variable which are used for google Oauth2."""

CLIENT_SECRETS_FILE = "client_secret.json"
SCOPES = [
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/userinfo.profile",
    "openid",
]
API_SERVICE_NAME = "oauth2"
API_VERSION = "v2"
CALLBACK_URI = "http://localhost:8000/callback/"
CLIENT_ID = "736704924478-2d8eoa7dq4amb8e3o0m0h63rgofurc56.apps.googleusercontent.com"
BASE_URI = "http://localhost:8000/"
