from django.shortcuts import redirect
from django.http import JsonResponse

import google.oauth2.credentials
from .utils import credentials_to_dict


def index(request):
    """Return some information."""
    return JsonResponse(json_response())


def test_api(request):
    """Testing the api."""
    if not request.session.get("credentials"):
        return redirect("authorize")
    credentials = google.oauth2.credentials.Credentials(
        **request.session["credentials"]
    )
    request.session["credentials"] = credentials_to_dict(credentials)
    res = json_response()
    return JsonResponse(res)


def json_response():
    """Return json response."""
    url = "http://localhost:8000/"
    res = {
        "test": url + "test",
        "revoke": url + "revoke",
        "clear": url + "clear",
        "authorize": url + "authorize",
        "logout": url + "logout",
    }

    return res


# need to add decorator
def logout(request):
    """
    Logout.

    No need to grant permission again will be saved for future use
    User can access again because state is not destroyed.
    """
    if request.session.get("credentials"):
        del request.session["credentials"]
    res = {
        "msg": "Logged out successfully",
        "name": request.session.get("name"),
    }

    return JsonResponse(res)
