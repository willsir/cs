from django.contrib.auth import get_user_model
from django.db import models
from ticket.models import Ticket
from .views import get_title

USER = get_user_model()

UPLOAD_TO = "uploads/% Y/% m/% d/"


class Conference(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    ticket = models.OneToOneField(
        to=Ticket, on_delete=models.PROTECT, null=True, blank=True
    )
    schedule_date = models.DateTimeField(null=True, blank=True)
    closed_date = models.DateTimeField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    staff = models.ForeignKey(
        to=USER, related_name="staff", on_delete=models.PROTECT, null=True, blank=True
    )
    price = models.IntegerField("Ticket Price", default=0)
    discount = models.IntegerField("Discount", default=0)
    author = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to=f"{UPLOAD_TO}{get_title(title)}/images/")
    document = models.FileField(upload_to=f"{UPLOAD_TO}{get_title(title)}/documents/")
    pdf = models.FileField(upload_to=f"{UPLOAD_TO}{get_title(title)}/pdfs/")

    def __str__(self):
        return self.staff.username
