from django.core.exceptions import PermissionDenied
import graphene
from graphene_django import DjangoObjectType
from graphene_file_upload.scalars import Upload
from .models import Conference
from ticket.models import Ticket
from ticket.schema import TicketType


class ConferenceType(DjangoObjectType):
    class Meta:
        model = Conference
        fields = ("title", "ticket", "description", "schedule_date")


class ConferenceQuery(graphene.ObjectType):
    conferences = graphene.List(ConferenceType)

    def resolve_conferences(
        root,
        info,
    ):
        return Conference.objects.all()


class CreateConference(graphene.Mutation):
    class Arguments:
        title = graphene.String(required=False)
        schedule_date = graphene.DateTime(required=False)
        closed_date = graphene.DateTime(required=False)
        description = graphene.String(required=False)
        image = Upload(required=False)
        decument = Upload(required=False)
        pdf = Upload(required=False)

    success = graphene.Boolean()
    msg = graphene.String()
    ticket_code = graphene.Field(TicketType)

    def mutate(root, info, **kwargs):
        title = kwargs.get("title")
        schedule_date = kwargs.get("schedule_date")
        closed_date = kwargs.get("closed_date")
        description = kwargs.get("description")
        image = kwargs.get("image")
        decument = kwargs.get("decument")
        pdf = kwargs.get("pdf")

        user = info.context.user
        try:
            if user.is_anonymous:
                raise PermissionDenied
            if not user.is_staff:
                raise PermissionDenied
            conference, _ = Conference.objects.get_or_create(staff=user)
            conference.title = title
            conference.schedule_date = schedule_date
            conference.closed_date = closed_date
            conference.description = description
            conference.image = image
            conference.decument = decument
            conference.pdf = pdf
            ticket, _ = Ticket.objects.get_or_create(owner=user)
            conference.ticket = ticket
            conference.save()
            return CreateConference(
                success=True, msg="Conference created", ticket_code=ticket
            )
        except PermissionDenied:
            return CreateConference(success=False, msg="No Permission", ticket_code=None)
        except Exception:
            return CreateConference(success=False, msg="Other Error", ticket_code=None)


class ConferenceMutation(graphene.ObjectType):
    create_conference = CreateConference.Field()


class Query(ConferenceQuery, graphene.ObjectType):
    "Conference Query"
    pass


class Mutation(ConferenceMutation, graphene.ObjectType):
    "Conference Mutation"
    pass


# schema = graphene.Schema(query=Query, mutation=Mutation)
