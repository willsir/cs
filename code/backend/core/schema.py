"""Graphql schema for project."""
import graphene
from users.schema import Query as UserQuery, Mutation as UserMutation
from ticket.schema import Query as TicketQuery, Mutation as TicketMutation
from conference.schema import Query as ConferenceQuery, Mutation as ConferenceMutation


class Query(UserQuery, TicketQuery, ConferenceQuery, graphene.ObjectType):
    """Project Graphql Query."""

    pass


class Mutation(UserMutation, TicketMutation, ConferenceMutation, graphene.ObjectType):
    """Project Graphql Mutation."""

    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
