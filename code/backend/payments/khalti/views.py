from os import getenv
import requests
from ticket.models import Ticket


KHALTI_PRIVATE = getenv("KHALTI_PRIVATE")


def verify(**kwargs):
    price = kwargs.get("price")
    ticket = kwargs.get("ticket")
    token = kwargs.get("token")
    number = kwargs.get("number")
    if number or number <= 1:
        number = 1
    amount = price * number * 100

    url = "https://khalti.com/api/v2/payment/verify/"
    payload = {"token": token, "amount": amount}
    headers = {"Authorization": f"Key {KHALTI_PRIVATE}"}
    ticket_obj = Ticket.objects.get(ticket=ticket)
    response = requests.post(url, payload, headers=headers)
    resp_dict = response.json()
    if resp_dict.get("idx"):
        return {"response": resp_dict, "ticket": ticket_obj}
    else:
        return None
