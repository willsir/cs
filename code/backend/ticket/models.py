from django.db import models
from django.contrib.auth import get_user_model
from .views import generate_ticket

USER = get_user_model()


class Ticket(models.Model):
    """
    ticket for the conference:
    ticket will be send after the user pay for it
    """

    title = models.CharField("Title", max_length=255, null=True, blank=True)
    ticket_code = models.CharField(
        "Ticket",
        max_length=100,
        default=generate_ticket(title),
        null=True,
        blank=True,
    )
    number = models.IntegerField("Number of Ticket", default=0)
    owner = models.ForeignKey(
        USER,
        related_name="owner",
        blank=True,
        null=True,
        verbose_name="Owner",
        on_delete=models.CASCADE,
    )
    buyer = models.ForeignKey(
        USER,
        related_name="buyer",
        blank=True,
        null=True,
        verbose_name="Buyer",
        on_delete=models.PROTECT,
    )
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True, blank=True)
    open_date = models.DateTimeField(blank=True, null=True)
    closed_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        try:
            return self.owner.username
        except AttributeError:
            return "None Ticket"
