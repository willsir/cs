from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
import graphene
from graphene_django import DjangoObjectType
from .models import Ticket
from payments.khalti.views import verify

CustomUser = get_user_model()


class TicketType(DjangoObjectType):
    class Meta:
        model = Ticket
        fields = "__all__"


class TicketQuery(graphene.ObjectType):
    tickets = graphene.List(TicketType)

    def resolve_tickets(
        root,
        info,
    ):
        return Ticket.objects.all()


class Query(TicketQuery, graphene.ObjectType):
    """
    Ticket Query
    """

    pass


class BuyTicket(graphene.Mutation):
    class Arguments:
        ticket = graphene.String()
        token = graphene.String()
        price = graphene.Int()
        number = graphene.Int()
        username = graphene.String()

    success = graphene.Boolean()
    msg = graphene.JSONString()
    number = graphene.Int()
    username = graphene.String()
    ticket = graphene.String()

    def mutate(root, info, **kwargs):
        ticket = kwargs.get("ticket")
        number = kwargs.get("number")

        user = info.context.user
        try:
            if not user.is_authenticated:
                raise PermissionDenied
            t = Ticket.objects.get(ticket=ticket)
            t.buyer = user
            v = verify(**kwargs)
            response, ticket_obj = v.get("response"), v.get("ticket")
            t.save()
            return BuyTicket(
                success=True,
                user=user,
                number=number,
                msg=response,
                ticket=ticket_obj,
                username=user.username,
            )
        except PermissionDenied:
            return BuyTicket(success=False, user=None, number=0, msg=None)
        except Exception:
            return BuyTicket(success=False, user=None, number=0, msg="Unknown Error")


class CreateTicket(graphene.Mutation):
    class Arguments:
        title = graphene.String()
        number = graphene.Int(required=False)
        open_date = graphene.DateTime(required=False)
        closed_date = graphene.DateTime(required=False)

    success = graphene.Boolean()
    msg = graphene.String()

    def mutate(root, info, **kwargs):
        title = kwargs.get("title")
        open_date = kwargs.get("open_date")
        closed_date = kwargs.get("closed_date")
        user = info.context.user
        try:
            if not user.is_authenticated:
                raise PermissionDenied
            if not user.is_staff:
                raise PermissionDenied
            ticket, _ = Ticket.objects.get_or_create(owner=user, title=title)
            ticket.open_date = open_date
            ticket.closed_date = closed_date
            ticket.save()
            return CreateTicket(success=True, msg="Ticket Created")
        except PermissionDenied:
            return CreateTicket(success=False, msg="No Permission")
        """ except Exception:
            return CreateTicket(success=False, msg="Unknown Error") """


class TicketMutation(graphene.ObjectType):
    create_ticket = CreateTicket.Field()
    buy_ticket = BuyTicket.Field()


class Mutation(TicketMutation, graphene.ObjectType):
    "Ticket Mutation"
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
