def generate_ticket(text="Default"):
    from uuid import uuid4
    from datetime import datetime

    text_list = [text[:2] for text in str(text).split()]
    uuid_first = str(uuid4()).split("-")[0]
    date_first = str(datetime.now()).split()[0].split("-")
    date_text = "".join(date_first)
    text_join = "".join(text_list)
    return text_join + uuid_first + date_text
