from graphene import ObjectType, List
from .types import UserProfileType, UserInterestType
from ..models import UserProfile, UserInterest


class UserProfileQuery(ObjectType):
    """
    user profile query
    """

    user_profiles = List(UserProfileType)

    def resolve_user_profiles(root, info, **kwargs):
        return UserProfile.objects.all()


class UserInterestQuery(ObjectType):
    """
    user profile query
    """

    user_interests = List(UserInterestType)

    def resolve_user_interests(root, info, **kwargs):
        return UserInterest.objects.all()
