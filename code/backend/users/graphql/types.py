from graphene_django import DjangoObjectType
from ..models import CustomUser, UserProfile, UserInterest


class CustomUserType(DjangoObjectType):
    class Meta:
        model = CustomUser


class UserProfileType(DjangoObjectType):
    def resolve_photo(self, info):
        """Resolve product photo absolute path"""
        if self.photo:
            self.photo = info.context.build_absolute_uri(self.photo.url)
        return self.photo

    class Meta:
        model = UserProfile
        fields = (
            "date_of_birth",
            "address",
            "city",
            "country",
            "gender",
            "mobile_phone",
            "role",
            "profession",
        )


class UserInterestType(DjangoObjectType):
    class Meta:
        model = UserInterest
        fileds = ("interest", "domain")
