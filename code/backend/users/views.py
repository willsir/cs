"""User related response."""
from django.shortcuts import HttpResponse
from .models import CustomUser


def index(request):
    """"""
    return HttpResponse("Welcome to homepage")


def verify(request, code):
    """Verify user and map to its userprofile."""
    try:
        verfication_check_query = """
          query {
            users(username: $username){
              edges{
                node{
                  verified
                }
              }
            }
          }
        """
        # if user is verified then only
        # map user with userprofile
        verification_query = """
          mutation {
            verifyToken(
              token: $code
            ){
              payload
              success
              errors
            }
          }
        """
    except CustomUser.userprofile.RelatedObjectDoesNotExist:

        # on success we receive
        pass
