import { createClient } from "@urql/core";
import { GLOBAL } from "../../global";
import { REGISTER, GET_TOKEN } from "../gql";

const client = createClient({
  url: GLOBAL.URL,
});

class AuthService {
  async login(user) {
    let res;
    try {
      res = await client
        .mutation(GET_TOKEN, user)
        .toPromise()
        .then((result) => {
          const token = result.data.tokenAuth.token;
          const username = result.data.tokenAuth.user.username;
          return { username, token };
        });
    } catch (err) {
      console.error("auth err", err);
      return {
        username: "",
        token: "",
      };
    }
    if (res.token) {
      const { username, token } = res;
      localStorage.setItem("user", JSON.stringify({ username, token }));
    }
    return res;
  }

  logout() {
    localStorage.removeItem("user");
  }

  async register(user) {
    let res;
    try {
      res = await client
        .mutation(REGISTER, user)
        .toPromise()
        .then((result) => {
          console.log(result.data.register.success);

          const { token, success, errors } = result.data.register;
          if (!success) {
            return { errors, success };
          }
          return { errors, success, token };
        });
    } catch (err) {
      console.log(err);
    }
    return res;
  }
}

export default new AuthService();
