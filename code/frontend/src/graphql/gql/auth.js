const GET_TOKEN= `
  mutation ($username: String!, $password: String!){
      tokenAuth(username: $username, password: $password){
          token
          success
          errors
              user {
                  username
              }
      }
  }
`;

const REGISTER = `
  mutation ($username: String!, $email: String!, $password1: String!, $password2: String!) {
    register(username:$username, email: $email, password1: $password1, password2: $password2){
      success
      errors
      refreshToken
      token
    }
  }
`;


export { GET_TOKEN, REGISTER };
