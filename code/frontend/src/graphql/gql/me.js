const GET_ME = `
  query {
    me {
    	username
      id
      email
      verified
    }
  }
`;

export { GET_ME };
