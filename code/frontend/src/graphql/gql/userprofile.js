const GET_MY_PROFILE = `
  query{
    me {
      id
      username
      firstName
      lastName
      email
      dateJoined
      userprofile {
        gender
        dateOfBirth
        address
        city
        country
        gender
        mobilePhone
        role
        profession
      }
    }
    userInterests {
      interest
      domain
    } 
  }
`;

const UPDATE_PROFILE = `

`;


export { GET_MY_PROFILE };
