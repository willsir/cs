import { createClient } from "@urql/core";
import { GLOBAL } from "../global";
import { GET_ME } from "./gql";

const client = createClient({
  url: GLOBAL.URL,
  requestPolicy: "cache-and-network",
  fetchOptions: () => {
    const token = getToken();
    return {
      headers: { authorization: token ? `JWT ${token}` : "" },
    };
  },
});

const testMe = (token) => {
  client
    .query(GET_ME)
    .toPromise()
    .then((result) => {
      if (!result.data.me) {
        localStorage.removeItem("user");
        window.location.pathname = "/login";
      }
    });
};

export const getToken = function () {
  const local = localStorage.getItem("user");
  if (local == null) {
    window.location.pathname = "/login";
    return null;
  }
  const { token } = JSON.parse(local);
  testMe(token);
  if (token) return token;
  else return null;
};
