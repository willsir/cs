import { createClient } from "@urql/core";
import { GLOBAL } from "../../global";
import { GET_ME } from "../gql/me";

const client = createClient({
  url: GLOBAL.URL,
});

async function getMe(user) {
  return await client
    .query(GET_ME)
    .toPromise()
    .then((result) => {
      return result.data.me;
    });
}

export { getMe };
