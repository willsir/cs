import "ant-design-vue/dist/antd.css";
import { createApp } from "vue";
import { DatePicker } from "ant-design-vue";
import urql, {
  dedupExchange,
  fetchExchange,
  cacheExchange,
  errorExchange,
} from "@urql/vue";
import App from "./App.vue";
import { router } from "./router";
import store from "./store";
import { getToken } from "./graphql";
import { GLOBAL } from "./global";

const app = createApp(App);
app.use(router);
app.use(DatePicker);
app.use(urql, {
  url: GLOBAL.URL,
  requestPolicy: "cache-and-network",
  exchanges: [
    cacheExchange,
    dedupExchange,
    errorExchange({
      onError(error) {
        console.error(error);
      },
    }),
    fetchExchange,
  ],
  fetchOptions: () => {
    const token = getToken();
    console.log("main", token);
    return {
      headers: { authorization: token ? `JWT ${token}` : "" },
    };
  },
});
app.use(store);
app.mount("#app");
