import { createRouter, createWebHistory } from "vue-router";
import { onBeforeRouteLeave, onBeforeRouteUpdate } from "vue-router";
import { routers } from "./router";

const router = createRouter({
  history: createWebHistory(),
  routes: routers,
});

// router.beforeEach((to, from, next) => {
//   if (to.meta.requiresAuth) {
//     // this route requires auth, check if logged in
//     // if not, redirect to login page.
//     return {
//       path: "/login",
//       // save the location we were at to come back later
//       query: { redirect: to.fullPath },
//     };
//   }
//   console.log("here router", to.meta.requiresAuth);
//   // router.push("home");
//   console.log(to.meta);
//   console.log(from);
//   console.log(next);
//   next();
// });

export { router };
