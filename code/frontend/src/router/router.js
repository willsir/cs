import Main from "../views/Main.vue";
import Conference from "../views/Main/Conference.vue";
import Home from "../views/Main/Home.vue";
import Query from "../views/Main/Query.vue";
import Session from "../views/Main/Session.vue";
import Record from "../views/Main/Record.vue";
import Store from "../views/Main/Store.vue";
import Profile from "../views/User/Profile.vue";
import Setting from "../views/User/Setting.vue";
import FileNotFound from "../views/Html/FileNotFound.vue";
import Login from "../views/Auth/Login.vue";
import Index from "../views/Main/Index.vue";
import Register from "../views/Auth/Register.vue";
import Test from "../views/Test.vue";

const routers = [
  {
    name: "root",
    path: "",
    component: Index,
  },
  {
    name: "test",
    path: "/test",
    component: Test,
  },
  {
    name: "login",
    path: "/login",
    component: Login,
  },
  {
    name: "store",
    path: "/store",
    components: {
      Store,
    },
  },

  {
    component: Main,
    meta: { requiresAuth: true },
    children: [
      {
        name: "stream",
        path: "/stream",
        components: { Conference },
      },
      {
        name: "store",
        path: "/store",
        components: { Store },
      },
      {
        name: "home",
        path: "/home",
        components: { Home },
      },
      {
        name: "query",
        path: "/query",
        components: { Query },
      },
      {
        name: "sessions",
        path: "/sessions",
        components: { Session },
      },
      {
        name: "record",
        path: "/record",
        components: { Record },
      },
      {
        name: "store",
        path: "/store",
        components: { Store },
      },
      {
        name: "profile",
        path: "/profile",
        components: { Profile },
      },
      {
        name: "setting",
        path: "/setting",
        components: { Setting },
      },
    ],
  },
  {
    path: "/:pathMatch(.*)",
    component: FileNotFound,
  },
];

export { routers };
