import AuthService from "../../graphql/auth";

const user = JSON.parse(localStorage.getItem("user"));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: {} };
const error = {
  login: {
    error: "",
  },
  register: {
    error: "",
  },
};
const state = { ...initialState, isVerified: false, ...error };

const getters = {
  user: (state) => ({ username: state.user.username, token: state.user.token }),
  loggedIn: (state) => state.status.loggedIn,
  loginError: (state) => state.login.error,
  registerError: (state) => state.register.error,
};

const actions = {
  login({ commit }, user) {
    return AuthService.login(user).then(
      (user) => {
        commit("LOGIN_SUCCESS", user);
        return Promise.resolve(user);
      },
      (error) => {
        commit("LOGIN_FAILURE");
        return Promise.reject(error);
      }
    );
  },
  logout({ commit }) {
    AuthService.logout();
    commit("LOGOUT");
  },
  register({ commit }, user) {
    return AuthService.register(user).then(
      (user) => {
        if (user.errors === null) commit("REGISTER_FAILURE", user.errors);
        commit("PARTIAL_REGISTER_SUCCCESS", user);
        return Promise.resolve(user);
      },
      (error) => {
        commit("REGISTER_FAILURE");
        return Promise.reject(error);
      }
    );
  },
};

const mutations = {
  LOGIN_SUCCESS(state, user) {
    state.status.loggedIn = true;
    state.user = user;
  },
  LOGIN_FAILURE(state) {
    state.status.loggedIn = false;
    state.user = null;
  },
  LOGOUT(state) {
    state.status.loggedIn = false;
    state.user = null;
  },
  PARTIAL_REGISTER_SUCCCESS(state, user) {
    console.log("state", state);
    state.user.token = user.token;
    state.status.loggedIn = false;
  },
  REGISTER_SUCCCESS(state) {
    console.log("state", state);
    state.status.isVerified = true;
  },
  REGISTER_FAILURE(state, payload) {
    console.log("pa", payload);
    state.status.loggedIn = false;
    state.register.error = { ...payload };
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
